// Відповіді на питання:

// 1.	Опишіть своїми словами як працює метод forEach.
// Метод forEach запускає функцію-коллбек, яка виконується для кожного елемента масиву.


// 2.	Як очистити масив?
// Найпростіший метод очистити масив – це arr.length = 0; А також можливо назначити пустий масив let arr = [];


// 3.	Як можна перевірити, що та чи інша змінна є масивом?
// Існує метод Array.isArray(value)  який повертає true якщо value – це мисв, або false якщо ні.


// Завдання №1


function filterBy(arr, type) {
  return arr.filter(item => typeof item !== type);
}

console.log(filterBy(['hello', 'world', 23, '23', null, true], 'string'));
console.log(filterBy(['hello', 'world', 23, '23', null, true], 'number'));
console.log(filterBy(['hello', 'world', 23, '23', null, true], 'boolean'));